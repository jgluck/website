jQuery(function($) {
  // alert("loaded")
  var is_touch_device = 'ontouchstart' in document.documentElement;
  var options = {
    $menu: false,
    menuSelector: 'a',
    panelSelector: 'section',
    namespace: '.panelSnap',
    onSnapStart: function() {},
    onSnapFinish: function() {},
    onActivate: function() {},
    directionThreshold: 50,
    slideSpeed: 200,
    keyboardNavigation: {
      enabled: true,
      nextPanelKey: 40,
      previousPanelKey: 38,
      wrapAround: true
    }
  };

  if (!is_touch_device) {
    $('body').panelSnap(options);
  }


});



// addLoadEvent(function() {
//    if (document.getElementById && document.getElementsByTagName) {
//         var aImgs = document.getElementById("body").getElementsByTagName("img");
//         imgSizer.collate(aImgs);
//    }
// });

function addLoadEvent(func) {
  var oldonload = window.onload;
  if (typeof window.onload != 'function') {
    window.onload = func;
  } else {
    window.onload = function() {
      if (oldonload) {
        oldonload();
      }
      func();
    }
  }
}

function open_github(func) {
  var win = window.open('http://github.com/jgluck', '_blank');
  win.focus();
}

function open_resume(func) {
  //var win = window.open(
  //  'https://www.dropbox.com/s/1oq6e1soy3svek2/JGluckResume.pdf?dl=0',
  // '_blank');
 var win = window.open(
    'files/resume.pdf',
    '_blank');
 
win.focus();
}

function select_cflow(func) {
  to_insert =
    "<h3>C-Flow: Consumer Traffic Visualization</h3> \
  </br>  <p> Created a tool and conducted a usability study with \
  the help of the employees at MicroStrategy in order to create an \
  interactive prototype HTML5 web visualization that can be adapted to \
  company needs.</p>\
  </br> <p>Additional Info: <a target='_blank' \
  href='https://wiki.cs.umd.edu/cmsc734_f13/index.php?title=\
  C-Flow:_Visualizing_Foot_Traffic_and_Profit_Data_to_Make_Informative_Decisions'>\
  Here</a></p>"

  p_text = document.getElementById("project_text");
  p_text.innerHTML = to_insert
    //p_text.innerHTML = "<p>CFLOW SELECTED</p>"

}

function select_ballpit(func) {

  to_insert =
    "<h3>Ballpit: HTML5 Node.Js Socket.io toy</h3> \
  </br>  <p> Cross platform physics toy utilizing many device specific\
   features. Balls travel between devices using websockets. \
   More detailed options can be accessed by hitting the \
   <i>Z</i> key</p>\
  </br> <p>Link To Ballpit: <a target='_blank' \
  href='http://amesbielenberg.com/BallPit/'>\
  Here</a></p>\
  </br> <p> Created with: <a target='_blank' \
  href='http://amesbielenberg.com'>Ames Bielenberg</a></p>"
  p_text = document.getElementById("project_text");
  p_text.innerHTML = to_insert
}

function select_rememberwhen(func) {
  to_insert =
    "<h3>RememberWhen</h3> \
  </br>  <p>Google Glass application for 'memorizing' and later 'refreshing'\
  memories in the form of geotagged photographs. Project \
  created for CMSC838F: Tangible computing.</p>\
  </br> <p>Additional Information: <a target='_blank' \
  href='http://cmsc838f-s14.wikispaces.com/RememberWhen'>\
  Here</a></p>\
  </br> <p> Created with: <a target='_blank' \
  href='http://www.kentwills.com/'>Kent Wills</a></p>"
  p_text = document.getElementById("project_text");
  p_text.innerHTML = to_insert
}

function select_cuff(func) {
  to_insert =
    "<h3>Cuff</h3> \
  </br>  <p>Novel game controller based on restricting players' movement\
  as well as measuring their biometrics to create a tense environment.\
  Paired with an HTML5 asteroids game. Project created for CMSC838F.</p>\
  </br> <p>Additional Information: <a target='_blank' \
  href='http://cmsc838f-s14.wikispaces.com/Cuff'>\
  Here</a></p>\
  </br> <p> Created with: <a target='_blank' \
  href=''>Tiffany Chao</a></p>"

  p_text = document.getElementById("project_text");
  p_text.innerHTML = to_insert
}

function select_pingstorm(func) {
  to_insert =
    "<h3>Pingstorm</h3> \
  </br>  <p>Utilized storm architecture to create a network health analysis \
  system for the realtime analysis of previously warehoused data. This allowed\
  on demand requests to be processed.</p>\
  </br> <p>Document Available: <a target='_blank' \
  href='http://www.sccs.swarthmore.edu/users/12/jgluck/Files/Documents/pingstorm-bringin-pingin.pdf'>\
  Here</a></p>"

  p_text = document.getElementById("project_text");
  p_text.innerHTML = to_insert
}

function select_chunksafe(func) {
  to_insert =
    "<h3>Chunk Safe</h3> \
  </br>  <p>Distributed backup system written in Python using the Entangled DHT\
   and the Twisted networking engine. Designed to passively backup scratch\
   drives with unused space.</p>\
  </br> <p>Projcet Source: <a target='_blank' \
  href='https://github.com/jgluck/ChunkSafe'>\
  Here</a></p>\
  </br> <p> Created with: <a target='_blank' \
  href='http://benjaminlipton.com/'>Benjamin Lipton</a></p>"
  p_text = document.getElementById("project_text");
  p_text.innerHTML = to_insert
}
